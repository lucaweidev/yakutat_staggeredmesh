#include <iostream>
#include <cmath>
#include <omp.h>
#include <vector>
#include "Resolution.h"
#include "MPI_tools.hpp"

using std::cout;
using std::endl;
using std::vector;

void DiscretisationQUICK
    (
    // ======================================================== //
    MPI_tools &U_MPI,
    size_t num_threads,
    double dt, double nu,

    vector<vector<double> > &U,
    vector<vector<double> > &U_star,
    vector<vector<double> > &NEIBcell,

    vector<double> &iDx,
    vector<double> &Dxs,
    vector<double> &iDy,
    vector<double> &Dys,
    vector<double> &iDz,
    vector<double> &Dzs

    
    // ======================================================== //
    )
{
    // ======================================================== //
    double u_tilde_x1, u_tilde_x2, u_tilde_y1, u_tilde_y2, u_tilde_z1, u_tilde_z2 ;

    double v_tilde_x1, v_tilde_x2, v_tilde_y1, v_tilde_y2, v_tilde_z1, v_tilde_z2;
    
    double w_tilde_x1, w_tilde_x2, w_tilde_y1, w_tilde_y2, w_tilde_z1, w_tilde_z2;

    double ue, uw, un, us, uf, ub, vnu, vsu, wfu, wbu;
    
    double ve, vw, vn, vs, vf, vb, uev, uwv, wfv, wbv;
    
    double we, ww, wn, ws, wf, wb, uew, uww, vnw, vsw;

    int    xmm, xm, xp, xpp;

    int    ymm, ym, yp, ypp;

    int    zmm, zm, zp, zpp;

    int    icel;

    int omp_count{0};

    // ======================================================== //
    
    // omp_count = (ny-2*gCells)*(nx-2*gCells) / num_threads;
    #pragma omp parallel
    {

    
    for(size_t i = U_MPI.start; i < U_MPI.end+1; ++i )
    {
        #pragma omp for collapse(2) 
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {
                icel = i*nz*ny + j*nz + k;
                xmm = NEIBcell[6][icel];
                xm  = NEIBcell[0][icel];
                xp  = NEIBcell[1][icel];
                xpp = NEIBcell[7][icel]; 

                ymm = NEIBcell[8][icel];
                ym  = NEIBcell[2][icel];
                yp  = NEIBcell[3][icel];
                ypp = NEIBcell[9][icel];

                zmm = NEIBcell[10][icel];
                zm  = NEIBcell[4][icel];
                zp  = NEIBcell[5][icel];
                zpp = NEIBcell[11][icel];

                u_tilde_x1 = 0.5 * (U[0][xp]+  U[0][icel]); 
                u_tilde_x2 = 0.5 * (U[0][xm]+  U[0][icel]);
                v_tilde_x1 = 0.5 * (U[1][icel]+U[1][xp]);
                v_tilde_x2 = 0.5 * (U[1][ym]+  U[1][ym+(1*nz*ny)]);
                w_tilde_x1 = 0.5 * (U[2][xp]+  U[2][icel]);
                w_tilde_x2 = 0.5 * (U[2][zm]+  U[2][zm+(1*nz*ny)]);

                



                if(u_tilde_x1 > 0)
                    ue  = 0.5*(U[0][icel]     + U[0][xp]  ) -0.125*iDx[i+1]*iDx[i+1]/Dxs[i] *  \
                        ( (U[0][xp]   - U[0][icel]    ) / iDx[i+1] \
                        - (U[0][icel]     - U[0][xm]  ) / iDx[i]   );
                else
                    ue  = 0.5*(U[0][icel]     + U[0][xp]  ) -0.125*iDx[i+1]*iDx[i+1]/Dxs[i+1]* \
                            ( (U[0][xpp]   - U[0][xp]  ) / iDx[i+2] \
                            - (U[0][xp]   - U[0][icel]    ) / iDx[i+1] );

               

                if (u_tilde_x2 > 0)
                     uw  = 0.5*(U[0][xm]   + U[0][icel]    ) -0.125*iDx[i]*iDx[i]/Dxs[i-1]* \
                    (  (U[0][icel]     - U[0][xm]  ) / iDx[i]   \
                        - (U[0][xm]   - U[0][xmm]  ) / iDx[i-1] ) ;
                else
                    uw  = 0.5*(U[0][xm]   + U[0][icel]    ) -0.125*iDx[i]*iDx[i]/Dxs[i]* \
                    (   (U[0][xp]   - U[0][icel]    ) / iDx[i+1] \
                        - (U[0][icel]     - U[0][xm]  ) / iDx[i]   )  ;



                if (v_tilde_x1 > 0)
                    un  = 0.5*(U[0][icel]     + U[0][yp]  ) -0.125*Dys[j]*Dys[j]/iDy[j]* \
                    (   (U[0][yp]   - U[0][icel]    ) / Dys[j]  \
                        - (U[0][icel]     - U[0][ym]  ) / Dys[j-1])  ;
                
                else
                    un  = 0.5*(U[0][icel]     + U[0][yp] )-0.125*Dys[j]*Dys[j]/iDy[j+1]* \
                    (   (U[0][ypp]   - U[0][yp]  ) / Dys[j+1]\
                        - (U[0][yp]   - U[0][icel]    ) / Dys[j]  );


                if (v_tilde_x2 > 0)
                    us  = 0.5*(U[0][ym]   + U[0][icel]    ) -0.125*Dys[j-1]*Dys[j-1]/iDy[j-1]* \
                    (   (U[0][icel]     - U[0][ym]  ) / Dys[j-1]\
                        - (U[0][ym]   - U[0][ymm]  ) / Dys[j-2]) ;
                        
                else
                    us  = 0.5*(U[0][ym]   + U[0][icel]    ) -0.125*Dys[j-1]*Dys[j-1]/iDy[j]* \
                    (   (U[0][yp]   - U[0][icel]    ) / Dys[j]  \
                        - (U[0][icel]     - U[0][ym]  ) / Dys[j-1])  ;


                if (w_tilde_x1 > 0) 
                    uf  = 0.5*(U[0][icel]     + U[0][zp]  ) -0.125*Dzs[k]*Dzs[k]/iDz[k]* \
                    (   (U[0][zp]   - U[0][icel]    ) / Dzs[k]  \
                        - (U[0][icel]     - U[0][zm]  ) / Dzs[k-1]);

                else
                    uf  = 0.5*(U[0][icel]     + U[0][zp]  ) -0.125*Dzs[k]*Dzs[k]/iDz[k+1]* \
                    (   (U[0][zpp]   - U[0][zp]  ) / Dzs[k+1]\
                        - (U[0][zp]   - U[0][icel]    ) / Dzs[k]  );



                if (w_tilde_x2 > 0) 
                    ub  = 0.5*(U[0][zm]   + U[0][icel]    ) -0.125*Dzs[k-1]*Dzs[k-1]/iDz[k-1]* \
                    (   (U[0][icel]     - U[0][zm]  ) / Dzs[k-1]\
                        - (U[0][zm]   - U[0][zmm]  ) / Dzs[k-2])  ;
                    
                else
                    ub  = 0.5*(U[0][zm]   + U[0][icel]    ) -0.125*Dzs[k-1]*Dzs[k-1]/iDz[k]* \
                    (   (U[0][zp]   - U[0][icel]    ) / Dzs[k]  \
                        - (U[0][icel]     - U[0][zm]  ) / Dzs[k-1]);


                U_star[0][icel] = U[0][icel]-dt*(u_tilde_x1*ue-u_tilde_x2*uw) / Dxs[i] \
                                            -dt*(v_tilde_x1*un-v_tilde_x2*us) / iDy[j] \
                                            -dt*(w_tilde_x1*uf-w_tilde_x2*ub) / iDz[k] \

                +(nu)*dt*( (U[0][xp]-U[0][icel]) / iDx[i+1] - (U[0][icel]-U[0][xm]) / iDx[i]   ) / Dxs[i] \
                +(nu)*dt*( (U[0][yp]-U[0][icel]) / Dys[j]   - (U[0][icel]-U[0][ym]) / Dys[j-1] ) / iDy[j] \
                +(nu)*dt*( (U[0][zp]-U[0][icel]) / Dzs[k]   - (U[0][icel]-U[0][zm]) / Dzs[k-1] ) / iDz[k] ;

      
            }
        }
    }


    

    //v_star calculation (y component) 
    for(int i = U_MPI.start; i < U_MPI.end+1; ++i )
    {
        #pragma omp for collapse(2) 
        for(int j = gCells; j < ny-gCells; ++j )
        {
            for(int k = gCells; k < nz-gCells; ++k)
            {
                icel = i*nz*ny + j*nz + k;
                xmm = NEIBcell[6][icel];
                xm  = NEIBcell[0][icel];
                xp  = NEIBcell[1][icel];
                xpp = NEIBcell[7][icel]; 

                ymm = NEIBcell[8][icel];
                ym  = NEIBcell[2][icel];
                yp  = NEIBcell[3][icel];
                ypp = NEIBcell[9][icel];

                zmm = NEIBcell[10][icel];
                zm  = NEIBcell[4][icel];
                zp  = NEIBcell[5][icel];
                zpp = NEIBcell[11][icel];

                u_tilde_y1 = 0.5 * (U[0][icel]+U[0][yp]);
                u_tilde_y2 = 0.5 * (U[0][xm]+  U[0][xm+(1*nz)]); 
                v_tilde_y1 = 0.5 * (U[1][icel]+U[1][yp]);
                v_tilde_y2 = 0.5 * (U[1][ym]+  U[1][icel]);
                w_tilde_y1 = 0.5 * (U[2][icel]+U[2][yp]);
                w_tilde_y2 = 0.5 * (U[2][zm]+  U[2][zm+(1*nz)]); 

                if (u_tilde_y1 > 0)  
                    ve  = 0.5*(U[1][icel]     + U[1][xp]  ) -0.125*Dxs[i]*Dxs[i]/iDx[i]* \
                        (   (U[1][xp]   - U[1][icel]    ) / Dxs[i]  \
                            - (U[1][icel]     - U[1][xm]  ) / Dxs[i-1]);

                else
                    ve  = 0.5*(U[1][icel]     + U[1][xp]  ) -0.125*Dxs[i]*Dxs[i]/iDx[i+1]* \
                        (   (U[1][xpp]   - U[1][xp]  ) / Dxs[i+1]\
                            - (U[1][xp]   - U[1][icel]    ) / Dxs[i]  );  


                if (u_tilde_y2 > 0)  
                    vw  = 0.5*(U[1][xm]   + U[1][icel]    ) -0.125*Dxs[i-1]*Dxs[i-1]/iDx[i-1]* \
                        (   (U[1][icel]     - U[1][xm]  ) / Dxs[i-1]\
                            - (U[1][xm]   - U[1][xmm]  ) / Dxs[i-2]); 

                else 
                    vw  = 0.5*(U[1][xm]   + U[1][icel]    ) -0.125*Dxs[i-1]*Dxs[i-1]/iDx[i]* \
                        (   (U[1][xp]   - U[1][icel]    ) / Dxs[i]  \
                            - (U[1][icel]     - U[1][xm]  ) / Dxs[i-1]);  


                if (v_tilde_y1 > 0)  
                    vn  = 0.5*(U[1][icel]     + U[1][yp]  ) -0.125*iDy[j+1]*iDy[j+1]/Dys[j]* \
                        (   (U[1][yp]   - U[1][icel]    ) / iDy[j+1] \
                            - (U[1][icel]     - U[1][ym]  ) / iDy[j]   ); 
                    
                else
                    vn  = 0.5*(U[1][icel]     + U[1][yp]  ) -0.125*iDy[j+1]*iDy[j+1]/Dys[j+1]* \
                        (   (U[1][ypp]   - U[1][yp]  ) / iDy[j+2] \
                            - (U[1][yp]   - U[1][icel]    ) / iDy[j+1] ); 


                if (v_tilde_y2 > 0)  
                    vs  = 0.5*(U[1][ym]   + U[1][icel]    ) -0.125*iDy[j]*iDy[j]/Dys[j-1]* \
                        (   (U[1][icel]     - U[1][ym]  ) / iDy[j]   \
                            - (U[1][ym]   - U[1][ymm]  ) / iDy[j-1] ); 
                    
                else
                    vs  = 0.5*(U[1][ym]   + U[1][icel]    ) -0.125*iDy[j]*iDy[j]/Dys[j]* \
                        (   (U[1][yp]   - U[1][icel]    ) / iDy[j+1]\
                            - (U[1][icel]     - U[1][ym]  ) / iDy[j]  ); 



                if (w_tilde_y1 > 0)  
                    vf  = 0.5*(U[1][icel]     + U[1][zp]  ) -0.125*Dzs[k]*Dzs[k]/iDz[k]* \
                        (   (U[1][zp]   - U[1][icel]    ) / Dzs[k]  \
                            - (U[1][icel]     - U[1][zm]  ) / Dzs[k-1]);
                    
                else
                    vf  = 0.5*(U[1][icel]     + U[1][zp]  ) -0.125*Dzs[k]*Dzs[k]/iDz[k+1]* \
                        (   (U[1][zpp]   - U[1][zp]  ) / Dzs[k+1]\
                            - (U[1][zp]   - U[1][icel]    ) / Dzs[k]  );

                if (w_tilde_y2 > 0)  
                    vb  = 0.5*(U[1][zm]   + U[1][icel]    ) -0.125*Dzs[k-1]*Dzs[k-1]/iDz[k-1]* \
                        (   (U[1][icel]     - U[1][zm]  ) / Dzs[k-1]\
                            - (U[1][zm]   - U[1][zmm]  ) / Dzs[k-2]);
                    
                else
                    vb  = 0.5*(U[1][zm]   + U[1][icel]    ) -0.125*Dzs[k-1]*Dzs[k-1]/iDz[k]* \
                        (   (U[1][zp]   - U[1][icel]    ) / Dzs[k]  \
                            - (U[1][icel]     - U[1][zm]  ) / Dzs[k-1]);


                //nut = Viseff[i][j][k]

                U_star[1][icel] = U[1][icel]-dt*(u_tilde_y1*ve-u_tilde_y2*vw) / iDx[i] \
                                            -dt*(v_tilde_y1*vn-v_tilde_y2*vs) / Dys[j] \
                                            -dt*(w_tilde_y1*vf-w_tilde_y2*vb) / iDz[k] \

                                +(nu)*dt*( (U[1][xp]-U[1][icel]) / Dxs[i] - (U[1][icel] - U[1][xm]) / Dxs[i-1] ) / iDx[i] \
                                +(nu)*dt*( (U[1][yp]-U[1][icel]) / iDy[j+1] - (U[1][icel] - U[1][ym]) / iDy[j] ) / Dys[j] \
                                +(nu)*dt*( (U[1][zp]-U[1][icel]) / Dzs[k] - (U[1][icel] - U[1][zm]) / Dzs[k-1] ) / iDz[k] ;
    
            }
        }
    }


    //w_star calculation (z component)
    for(int i = U_MPI.start; i < U_MPI.end+1; ++i )
    {
        #pragma omp for collapse(2) 
        for(int j = gCells; j < ny-gCells; ++j )
        {
            for(int k = gCells; k < nz-gCells; ++k)
            {

                        icel = i*nz*ny + j*nz + k;
                        xmm = NEIBcell[6][icel];
                        xm  = NEIBcell[0][icel];
                        xp  = NEIBcell[1][icel];
                        xpp = NEIBcell[7][icel]; 

                        ymm = NEIBcell[8][icel];
                        ym  = NEIBcell[2][icel];
                        yp  = NEIBcell[3][icel];
                        ypp = NEIBcell[9][icel];

                        zmm = NEIBcell[10][icel];
                        zm  = NEIBcell[4][icel];
                        zp  = NEIBcell[5][icel];
                        zpp = NEIBcell[11][icel];


                        u_tilde_z1 = 0.5 * ( U[0][zp]  +U[0][icel] );
                        u_tilde_z2 = 0.5 * ( U[0][xm+1]+U[0][xm] ); 
                        v_tilde_z1 = 0.5 * ( U[1][icel]+U[1][zp] );
                        v_tilde_z2 = 0.5 * ( U[1][ym+1]+U[1][ym] );
                        w_tilde_z1 = 0.5 * ( U[2][zp]  +U[2][icel] );
                        w_tilde_z2 = 0.5 * ( U[2][icel]+U[2][zm] );

                        if (u_tilde_z1 > 0)
                        {
                            we  = 0.5*(U[2][icel]     + U[2][xp]  ) -0.125*Dxs[i]*Dxs[i]/iDx[i]* \
                                    ( (U[2][xp]       - U[2][icel]) / Dxs[i]  \
                                    - (U[2][icel]     - U[2][xm]  ) / Dxs[i-1]) ;

                        } 
                        else
                        {
                            we  = 0.5*(U[2][icel]     + U[2][xp]  ) -0.125*Dxs[i]*Dxs[i]/iDx[i+1]* \
                                    (  (U[2][xpp]   - U[2][xp]  ) / Dxs[i+1] \
                                     - (U[2][xp]   - U[2][icel]    ) / Dxs[i]   );
                        }
                            
                        

                        if (u_tilde_z2 > 0) 
                        {
                            ww  = 0.5*(U[2][xm]   + U[2][icel]    ) -0.125*Dxs[i-1]*Dxs[i-1]/iDx[i-1]* \
                                    (  (U[2][icel]     - U[2][xm]  ) / Dxs[i-1] \
                                    - (U[2][xm]   - U[2][xmm]  ) / Dxs[i-2] );
                        }
                        else 
                        {
                            ww  = 0.5*(U[2][xm]   + U[2][icel]    ) -0.125*Dxs[i-1]*Dxs[i-1]/iDx[i]* \
                                    (  (U[2][xp]   - U[2][icel]    ) / Dxs[i]  \
                                    - (U[2][icel]     - U[2][xm]  ) / Dxs[i-1]) ;
                        }
                            

                        

                        if (v_tilde_z1 > 0) 
                        {
                            wn  = 0.5*(U[2][icel]     + U[2][yp]  ) -0.125*Dys[j]*Dys[j]/iDy[j]* \
                                    (   (U[2][yp]   - U[2][icel]    ) / Dys[j]  \
                                        - (U[2][icel]     - U[2][ym]  ) / Dys[j-1]);
                        }
                        else
                        {
                            wn  = 0.5*(U[2][icel]     + U[2][yp]  ) -0.125*Dys[j]*Dys[j]/iDy[j+1]* \
                                    (   (U[2][ypp]   - U[2][yp]  ) / Dys[j+1]\
                                        - (U[2][yp]   - U[2][icel]    ) / Dys[j]  ) ;
                        }
                            

                        if (v_tilde_z2 > 0) 
                        {
                            ws  = 0.5*(U[2][ym]   + U[2][icel]    ) -0.125*Dys[j-1]*Dys[j-1]/iDy[j-1]* \
                                    (   (U[2][icel]     - U[2][ym]  ) / Dys[j-1]\
                                        - (U[2][ym]   - U[2][ymm]  ) / Dys[j-2]);
                        }
                        else
                        {
                            ws  = 0.5*(U[2][ym]   + U[2][icel]    ) -0.125*Dys[j-1]*Dys[j-1]/iDy[j]* \
                                    (   (U[2][yp]   - U[2][icel]    ) / Dys[j]  \
                                        - (U[2][icel]     - U[2][ym]  ) / Dys[j-1]);
                        }
                            


                        if (w_tilde_z1 > 0) 
                        {
                            wf  = 0.5*(U[2][icel]     + U[2][zp]  ) -0.125*iDz[k+1]*iDz[k+1]/Dzs[k]* \
                                    (   (U[2][zp]   - U[2][icel]    ) / iDz[k+1]\
                                        - (U[2][icel]     - U[2][zm]  ) / iDz[k]  );
                        }
                        else
                        {
                            wf  = 0.5*(U[2][icel]     + U[2][zp]  ) -0.125*iDz[k+1]*iDz[k+1]/Dzs[k+1]* \
                                    (   (U[2][zpp]   - U[2][zp]  ) / iDz[k+2]\
                                        - (U[2][zp]   - U[2][icel]    ) / iDz[k+1]);
                        }
                            
                            
                        if (w_tilde_z2 > 0) 
                        {
                            wb  = 0.5*(U[2][zm]   + U[2][icel]    ) -0.125*iDz[k]*iDz[k]/Dzs[k-1]* \
                                    (   (U[2][icel]     - U[2][zm]  ) / iDz[k]  \
                                        - (U[2][zm]   - U[2][zmm]  ) / iDz[k-1]);
                        }
                        else
                        {
                            wb  = 0.5*(U[2][zm]   + U[2][icel]    ) -0.125*iDz[k]*iDz[k]/Dzs[k]* \
                                        (  (U[2][zp]   - U[2][icel]    ) / iDz[k+1]\
                                    - (   U[2][icel]     - U[2][zm]  ) / iDz[k]  );

                        }
                            
                        
                        U_star[2][icel] = U[2][icel]-dt*(u_tilde_z1*we-u_tilde_z2*ww) / iDx[i] \
                                                -dt*(v_tilde_z1*wn-v_tilde_z2*ws) / iDy[j] \
                                                -dt*(w_tilde_z1*wf-w_tilde_z2*wb) / Dzs[k] \
                                            
                                    +(nu)*dt*( ( U[2][xp]-U[2][icel] ) / Dxs[i] -  ( U[2][icel]-U[2][xm] ) / Dxs[i-1]  ) / iDx[i] \
                                    +(nu)*dt*( ( U[2][yp]-U[2][icel] ) / Dys[j] -  ( U[2][icel]-U[2][ym] ) / Dys[j-1]  ) / iDy[j] \
                                    +(nu)*dt*( ( U[2][zp]-U[2][icel] ) / iDz[k+1] - ( U[2][icel]-U[2][zm] ) / iDz[k]   ) / Dzs[k] ;
  

            }
        }
    }


    }













}