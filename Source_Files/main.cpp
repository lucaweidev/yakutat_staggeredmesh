#include <fstream>
#include <iostream>
#include <string>
#include <omp.h>
#include <mpi.h>

#include "MPI_tools.hpp"
#include "gridder.h"
#include "readingData.h"
#include "BoundaryConditions.h"
#include "filer.h"
#include "ConvectionScheme.h"
#include "PressureSolvers.h"
#include "calNewVelocity.h"

using std::cout;
using std::endl;
using std::string;



int main(int argc, char **argv)
{   

    #include "Resolution.h"
    #include "Variables.h"

    // ============= MPI start ============= //

    // ======================================== //
	MPI_Status istat[8];					    //
												//
	MPI_Comm comm;								//
												//
	MPI_Init (&argc, &argv);					//
												//
	MPI_Comm_size(MPI_COMM_WORLD, &nproc);		//
												//
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);		//
												//
	comm = MPI_COMM_WORLD;				        //
    // ======================================== //

    if(myid == master)
    {
        #ifdef _OPENMP
        std::cout << "OpenMP: " << _OPENMP << std::endl;
        #else
        std::cout << "Your compiler does not support OpenMP." << std::endl;
        #endif
    }

    // ============= OPENMP ============= ／／
   num_threads = omp_get_max_threads();
   omp_set_num_threads(num_threads);
    // ============= OPENMP ============= ／／

    if(myid == master)
    {
        cout << "number of threads = " << num_threads << endl;
    }


    
    #include "array.h"



// Define neibor for MPI
// ======================================================== //
    lnbr = myid - 1;
    rnbr = myid + 1;
    if(myid == master ) lnbr = MPI_PROC_NULL;
    if(myid == nproc-1) rnbr = MPI_PROC_NULL;
// ======================================================== //


// MPI division
// ======================================================== //
    MPI_tools U_MPI;
    MPI_tools P_MPI;
    MPI_division(nx-2*gCells, gCells,  U_MPI.start, U_MPI.end, U_MPI.count,
                 U_MPI.start_list, U_MPI.end_list, U_MPI.count_list, myid, nproc);

    MPI_division((nx-2*gCells)* (ny-2*gCells) *(nz-2*gCells), 0,  P_MPI.start, P_MPI.end, P_MPI.count,
                 P_MPI.start_list, P_MPI.end_list, P_MPI.count_list, myid, nproc);
// ======================================================== //

    // cout << "myid: " << myid << "  Ustart: " << U_MPI.start << "  Uend: " << U_MPI.end << "  Ucount: " << U_MPI.count << endl;

    // MPI_Barrier(MPI_COMM_WORLD);

    // if(myid == master) cout << "===P_MPI===" << endl;

    // cout << "myid: " << myid << "  Pstart: " << P_MPI.start << "  Pend: " << P_MPI.end << "  Pcount: " << P_MPI.count << endl;

// ======================================================== // 

    Re = 100.0;

    dt = 1.0e-3;

    time = 0.0;

    nstep = 2000;

    isto = 1000;

    zeta = 1.0e-4; 

    itmax = 3000;

    omega = 1.8;

    //non-uniform 0, uniform 1 
    string Gridder = "uniform";            

    
// ======================================================== //

    for(size_t i = 0; i < iceltot; ++i )
    {
        U[0][i] = 0;
        U[1][i] = 0;
        U[2][i] = 0;
    }

    NEIBceller(NEIBcell);   


    readingData(dx, dy, dz, lx, ly, lz, lxSml, lySml, lzSml, Gridder, 
                nx, ny, nz, nxSml, nySml, nzSml, dxSml, dySml, dzSml,
                Re, nu);
    
                
    gridder(GridderXc, GridderYc, GridderZc, lxSml, lySml, lzSml, dx, dy, dz, dxSml,
            dySml, dzSml, Gridder, myid, iDx, Dxs, iDy, Dys, iDz, Dzs);

    createPressureMatrix(matA, iDx, Dxs, iDy, Dys, iDz, Dzs, P_MPI);


    BoundaryConditions(U, P);

    if(myid==master)
    {
        Output_Q_Cpp_PLOT3D(istep, P, U2, ETA);
    }






    for(istep = 1; istep < nstep+1; ++istep)
    {
        
        ////****  data transformation among nodes ****//// 
        // ======================================================== //
        islice = ny * nz;
        icount = 2 * islice;
        istart = U_MPI.start * islice;
        iend   = U_MPI.end * islice;

        itag = 110;
        MPI_Sendrecv(   (void *)&U[0][istart], icount, MPI_DOUBLE, lnbr, itag, \
                        (void *)&U[0][iend+islice], icount, MPI_DOUBLE, rnbr, itag, comm, istat    );

        itag = 120;
        MPI_Sendrecv(   (void *)&U[1][istart], icount, MPI_DOUBLE, lnbr, itag, \
                        (void *)&U[1][iend+islice], icount, MPI_DOUBLE, rnbr, itag, comm, istat    );

        itag = 130;
        MPI_Sendrecv(   (void *)&U[2][istart], icount, MPI_DOUBLE, lnbr, itag, \
                        (void *)&U[2][iend+islice], icount, MPI_DOUBLE, rnbr, itag, comm, istat    );


        itag = 140;
        MPI_Sendrecv(   (void *)&U[0][iend-islice], icount, MPI_DOUBLE, rnbr, itag, \
                        (void *)&U[0][istart-2*islice], icount, MPI_DOUBLE, lnbr, itag, comm, istat    );

        itag = 150;
        MPI_Sendrecv(   (void *)&U[1][iend-islice], icount, MPI_DOUBLE, rnbr, itag, \
                        (void *)&U[1][istart-2*islice], icount, MPI_DOUBLE, lnbr, itag, comm, istat    );

        itag = 160;
        MPI_Sendrecv(   (void *)&U[2][iend-islice], icount, MPI_DOUBLE, rnbr, itag, \
                        (void *)&U[2][istart-2*islice], icount, MPI_DOUBLE, lnbr, itag, comm, istat    );

        MPI_Barrier(MPI_COMM_WORLD);
        // ======================================================== //
        


        DiscretisationQUICK(U_MPI, num_threads, dt, nu, U, U_star, NEIBcell, iDx, Dxs, iDy, Dys, iDz, Dzs);


        ////****  data transformation among nodes ****//// 
        // ======================================================== //
        islice = ny * nz;
        icount = 2 * islice;
        istart = U_MPI.start * islice;
        iend   = U_MPI.end * islice;


        itag = 170;
        MPI_Sendrecv(   (void *)&U_star[0][iend-islice], icount, MPI_DOUBLE, rnbr, itag, \
                        (void *)&U_star[0][istart-2*islice], icount, MPI_DOUBLE, lnbr, itag, comm, istat    );

        itag = 180;
        MPI_Sendrecv(   (void *)&U_star[1][iend-islice], icount, MPI_DOUBLE, rnbr, itag, \
                        (void *)&U_star[1][istart-2*islice], icount, MPI_DOUBLE, lnbr, itag, comm, istat    );

        itag = 190;
        MPI_Sendrecv(   (void *)&U_star[2][iend-islice], icount, MPI_DOUBLE, rnbr, itag, \
                        (void *)&U_star[2][istart-2*islice], icount, MPI_DOUBLE, lnbr, itag, comm, istat    );

        MPI_Barrier(MPI_COMM_WORLD);
        // ======================================================== //

        createBMatrix(dt ,U_MPI , myid, nproc, NEIBcell, U_star, B_coo_val, iDx, Dxs, iDy, Dys, iDz, Dzs); //MPI X

        // data collect among nodes 
        // ======================================================== //
        islice = (ny-2*gCells) * (nz-2*gCells);
        icount = U_MPI.count * islice;
        istart = (U_MPI.start-gCells) * islice;

        
        if( myid > master )
        {
            itag = 9;
            MPI_Send(   (void *)&B_coo_val[istart], icount, MPI_DOUBLE, master, itag, MPI_COMM_WORLD );
        
        }
        else if(myid == master)
        {
            for(size_t i = 1 ; i < nproc ; ++i)
            {
                itag = 9;
                MPI_Recv(   (void *)&B_coo_val[ (U_MPI.start_list[i]-gCells)*islice ], icount, MPI_DOUBLE, i, itag, MPI_COMM_WORLD, istat  );
            
            }
        }
        
        icount = (nx-2*gCells)* (ny-2*gCells) *(nz-2*gCells);
        MPI_Bcast(  (void *)&B_coo_val[0], icount, MPI_DOUBLE, master, MPI_COMM_WORLD    );

        MPI_Barrier(MPI_COMM_WORLD);
        // ======================================================== //


        clock = MPI_Wtime();

        x = BiCGSTAB(P_MPI, myid, nproc, master, num_threads, zeta, itmax, matA, B_coo_val);

        clock = MPI_Wtime() - clock;

        readingMatrixData(x, P, U_MPI);


        ////****  data transformation among nodes ****//// 
        // ======================================================== //
        islice = ny * nz;
        icount = 1 * islice;
        istart = U_MPI.start * islice;
        iend   = U_MPI.end * islice;

        itag = 170;
        MPI_Sendrecv(   (void *)&P[istart], icount, MPI_DOUBLE, lnbr, itag, \
                        (void *)&P[iend+islice], icount, MPI_DOUBLE, rnbr, itag, comm, istat    );

        itag = 180;
        MPI_Sendrecv(   (void *)&ETA[istart], icount, MPI_DOUBLE, lnbr, itag, \
                        (void *)&ETA[iend+islice], icount, MPI_DOUBLE, rnbr, itag, comm, istat    );
        // ======================================================== //

        calNewVelocity(dt, U_MPI, P, U_star, U2, FX, ETA, iDx, Dxs, iDy, Dys, iDz, Dzs);
        
        updating(U, U2, U_MPI);

        BoundaryConditions(U, P);
        
        if(istep%isto==0)
        {
            // data collect among nodes to master
            // ======================================================== //
            islice = ny * nz;
            icount = U_MPI.count * islice;
            istart = U_MPI.start * islice;

            
            if( myid > master )
            {
                itag = 40;
                MPI_Send(   (void *)&U2[0][istart], icount, MPI_DOUBLE, master, itag, comm  );
                itag = 50;
                MPI_Send(   (void *)&U2[1][istart], icount, MPI_DOUBLE, master, itag, comm  );
                itag = 60;
                MPI_Send(   (void *)&U2[2][istart], icount, MPI_DOUBLE, master, itag, comm  );
                itag = 70;
                MPI_Send(   (void *)&P[istart], icount, MPI_DOUBLE, master, itag, comm  );
            }
            else if(myid == master)
            {
                for(size_t i = 1 ; i < nproc ; ++i)
                {
                    itag = 40;
                    MPI_Recv(   (void *)&U2[0][ U_MPI.start_list[i]*islice ], icount, MPI_DOUBLE, i, itag, comm, istat  );
                    itag = 50;
                    MPI_Recv(   (void *)&U2[1][ U_MPI.start_list[i]*islice ], icount, MPI_DOUBLE, i, itag, comm, istat  );
                    itag = 60;
                    MPI_Recv(   (void *)&U2[2][ U_MPI.start_list[i]*islice ], icount, MPI_DOUBLE, i, itag, comm, istat  );
                    itag = 70;
                    MPI_Recv(   (void *)&P[ U_MPI.start_list[i]*islice ], icount, MPI_DOUBLE, i, itag, comm, istat  );
                }
            }
            // ======================================================== //
        }
        


        if(istep%isto==0 && myid==master)
        {
            Output_Q_Cpp_PLOT3D(istep, P, U2, ETA);
        }
        

        if(myid ==master)
        {
            time += dt;
            cout << "time = " << time << " cost time = " << clock << endl;
            cout << "====================================================================" << endl;
            cout << endl;
        }
    }
    istep--;

// ============================================ //
MPI_Finalize();    /**** MPI-end ****/          //
// ============================================ //



    return 0;

}
