#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <omp.h>
#include "MPI_tools.hpp"

#include "Resolution.h"

using std::vector;


void readingData
    (
    // ======================================================== //
    double &dx, double &dy, double &dz, 
    const double lx, const double ly, const double lz, 
    const double lxSml, const double lySml, const double lzSml,
    std::string Gridder, 
    const int nx, const int ny, const int nz,
    const double nxSml, const double nySml, const double nzSml, 
    double &dxSml, double &dySml, double &dzSml,
    double Re, double &nu
    // ======================================================== //
    )
{
    if(Gridder == "non-uniform"){

        //Small interval
        dxSml = lxSml/nxSml;
        dySml = lySml/nySml;
        dzSml = lzSml/nzSml;


        //Large interval
        dx = ( lx-lxSml ) / ( (nx-2.0*gCells) - nxSml ); 
        dy = ( ly-lySml ) / ( (ny-2.0*gCells) - nySml );
        dz = ( lz-lzSml ) / ( (nz-2.0*gCells) - nzSml );


    }
    else if(Gridder == "uniform"){

        dx = lx / (nx-2.0*gCells); 
        dy = ly / (ny-2.0*gCells);
        dz = lz / (nz-2.0*gCells);

    }

       

    nu = 1./Re;

}



void NEIBceller
    (
    // ======================================================== //
    vector<vector<double> > &NEIBcell
    // ======================================================== //
    )
{
    // ======================================================== //
    
    int icel;

    // ======================================================== //
    
    using std::fstream;
    using std::ofstream;
    using std::endl;
    // ofstream file;
    // file.open("Output/NEIBcell.dat",ofstream::app);


    for(size_t i = gCells; i < nx-gCells; ++i )
    {
        
        for(size_t j = gCells; j < ny-gCells; ++j )
        {
            for(size_t k = gCells; k < nz-gCells; ++k)
            {

                icel = i*nz*ny + j*nz + k;

                NEIBcell[0][icel]  = icel - nz*ny; //-x
                NEIBcell[1][icel]  = icel + nz*ny; //+x
                NEIBcell[2][icel]  = icel - nz;    //-y
                NEIBcell[3][icel]  = icel + nz;    //+y
                NEIBcell[4][icel]  = icel - 1;     //-z
                NEIBcell[5][icel]  = icel + 1;     //+z

                NEIBcell[6][icel] = icel - 2*(nz*ny); //-2x
                NEIBcell[7][icel] = icel + 2*(nz*ny); //+2x
                NEIBcell[8][icel] = icel - 2*(nz);    //-2y
                NEIBcell[9][icel] = icel + 2*(nz);    //+2y
                NEIBcell[10][icel] = icel - 2*(1);     //-2z
                NEIBcell[11][icel] = icel + 2*(1);     //+2z

                // file<< icel \
                //     << "   " << NEIBcell[0][icel]\
                //     << "   " << NEIBcell[1][icel]\
                //     << "   " << NEIBcell[2][icel]\
                //     << "   " << NEIBcell[3][icel]\
                //     << "   " << NEIBcell[4][icel]\
                //     << "   " << NEIBcell[5][icel]\
                //     << " , " << NEIBcell[6][icel]\
                //     << "   " << NEIBcell[7][icel]\
                //     << "   " << NEIBcell[8][icel]\
                //     << "   " << NEIBcell[9][icel]\
                //     << "   " << NEIBcell[10][icel] \
                //     << "   " << NEIBcell[11][icel] \
                //     << " , " << i << " " << j << " " << k \
                //     << endl;

        
            }
        }
    }


    // file.close();

}






void readingMatrixData
    (
    // ======================================================== //
    vector<double> &x,
    vector<double> &P,
    MPI_tools U_MPI
    // ======================================================== //
    )
{
    // ======================================================== //
    size_t icel = 0;
    size_t ik = 0;
    size_t islice = 0;
    // ======================================================== //


    
    islice = (ny-2*gCells) * (nz-2*gCells);
    ik = (U_MPI.start-2) * islice;


    for(int i = U_MPI.start; i < U_MPI.end+1; ++i )
    {
        for(int j = gCells; j < ny-gCells; ++j )
        {
            for(int k = gCells; k < nz-gCells; ++k )
            {  
                icel = i*nz*ny + j*nz + k;

                P[icel] = x[ik];

                ++ik;
            }
        }
    }




}