#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "MPI_tools.hpp"


#include "Resolution.h"

using std::string;
using std::vector;

void BoundaryConditions
    (
    // ======================================================== //
    vector<vector<double> > &U,
    vector<double> &P
    // ======================================================== //
    )
{
    // ======================================================== //

    int icel;
    int i_index;
    int j_index;
    int k_index;

    // ======================================================== //



    //B.C
    // ======================================================== //

    //    y=1 ______________                                                                                 
    //       /             /|                                                     
    //      /       N     / |                                                          
    //     /____________ /  |                                
    //     |  |         |   |                                                        
    //     |  | B       |   |                                          
    //   W |  |         | E |                                           
    //     |  |_z=1_____|___|                                       
    //     |  /         |  /                                         
    //     | /     S    | /                                        
    //     |/___________|/  x=1                                        
    //  x=y=z=0   F     


    //   Neumann     du/dn = 0
    //   Dirichlet   u = 1
    //   no-slip     u = 0

    string WestWall_u         = "no-slip";
    string WestWall_v         = "no-slip";
    string WestWall_w         = "no-slip";
    
    string EastWall_u         = "no-slip";
    string EastWall_v         = "no-slip";
    string EastWall_w         = "no-slip";
    
    string SouthWall_u        = "no-slip";
    string SouthWall_v        = "no-slip";
    string SouthWall_w        = "no-slip";
    
    string NorthWall_u        = "Dirichlet";
    string NorthWall_v        = "no-slip";
    string NorthWall_w        = "no-slip";
    
    string BackWall_u         = "no-slip";
    string BackWall_v         = "no-slip";
    string BackWall_w         = "no-slip";
    
    string FrontWall_u        = "no-slip";
    string FrontWall_v        = "no-slip";
    string FrontWall_w        = "no-slip";

    // ======================================================== //


    

    // ======================================================== //
    //                                                          //
    //     Velocity Boundary conditions calculation             //
    //                                                          //
    // ======================================================== //

    
    for(size_t j = 0; j < ny; ++j){
        for(size_t k = 0; k < nz; ++k){

           
            //====================//
            // West vertical wall //
            //====================//
            i_index = 1*nz*ny;
            icel = i_index + j*nz + k;

            // =============================== //
            if(WestWall_u == "no-slip")
                U[0][icel] = 0.0;
            else if(WestWall_u == "Neumann")
                U[0][icel] = U[0][icel+1*nz*ny];
            else if(WestWall_u == "Dirichlet")
                U[0][icel] = 1.0;


            U[0][icel-1*nz*ny] = U[0][icel];
            // =============================== //


            // =============================== //
            if(WestWall_v == "no-slip")
                U[1][icel] = 2.0*0.0-U[1][icel+1*nz*ny];
            else if(WestWall_v == "Neumann")
                U[1][icel] = U[1][icel+1*nz*ny];
            else if(WestWall_v == "Dirichlet")
                U[1][icel] = 2.0*1.0-U[1][icel+1*nz*ny];


            U[1][icel-1*nz*ny] = U[1][icel];
            // =============================== //


            // =============================== //
            if(WestWall_w== "no-slip")
                U[2][icel] = 2.0*0.0-U[2][icel+1*nz*ny];
            else if(WestWall_w == "Neumann")
                U[2][icel] = U[2][icel+1*nz*ny];
            else if(WestWall_w == "Dirichlet")
                U[2][icel] = 2.0*1.0-U[2][icel+1*nz*ny];


            U[2][icel-1*nz*ny] = U[2][icel];
            // =============================== //



            //====================//
            // East vertical wall //
            //====================//
            i_index = (nx-3)*nz*ny;
            icel = i_index + j*nz + k;

            // =============================== //
            if(EastWall_u == "no-slip")
                U[0][icel] = 0.0;
            else if(EastWall_u == "Neumann")
                U[0][icel] = U[0][icel-1*nz*ny];
            else if(EastWall_u == "Dirichlet")
                U[0][icel] = 1.0;


            U[0][icel+1*nz*ny] =  U[0][icel];
            U[0][icel+2*nz*ny] = U[0][icel+1*nz*ny];
            // =============================== //


            // =============================== //
            if(EastWall_v == "no-slip")
                U[1][icel+1*nz*ny] = 2.0*0.0-U[1][icel];
            else if(EastWall_v == "Neumann")
                U[1][icel+1*nz*ny] = U[1][icel];
            else if(EastWall_v == "Dirichlet")
                U[1][icel+1*nz*ny] = 2.0*1.0-U[1][icel];


            U[1][icel+2*nz*ny] = U[1][icel+1*nz*ny];
            // =============================== //


            // =============================== //
            if(EastWall_w == "no-slip")
                U[2][icel+1*nz*ny] = 2.0*0.0-U[2][icel];
            else if(EastWall_w == "Neumann")
                U[2][icel+1*nz*ny] = U[2][icel];
            else if(EastWall_w == "Dirichlet")
                U[2][icel+1*nz*ny] = 2.0*1.0-U[2][icel];


            U[2][icel+2*nz*ny] = U[2][icel+1*nz*ny];
            // =============================== //

        }
    }



    for(size_t i = 0; i < nx; ++i ){
        for(size_t k = 0; k < nz; ++k ){

            //=====================//
            // South vertical wall //
            //=====================//
            j_index = 1*nz;
            icel = i*nz*ny + j_index + k;

            // =============================== //
            if(SouthWall_u == "no-slip")
                U[0][icel] = 2.0*0.0-U[0][icel+1*nz];
            else if(SouthWall_u == "Neumann")
                U[0][icel] = U[0][icel+1*nz];
            else if(SouthWall_u == "Dirichlet")
                U[0][icel] = 2.0*1.0-U[0][icel+1*nz];


            U[0][icel-1*nz] = U[0][icel];
            // =============================== //


            // =============================== //
            if(SouthWall_v == "no-slip")
                U[1][icel] = 0.0;
            else if(SouthWall_v == "Neumann")
                U[1][icel] = U[1][icel+1*nz];
            else if(SouthWall_v == "Dirichlet")
                U[1][icel] = 1.0;


            U[1][icel-1*nz] = U[1][icel];
            // =============================== //

            // =============================== //
            if(SouthWall_w == "no-slip")
                U[2][icel] = 2.0*0.0-U[2][icel+1*nz];
            else if(SouthWall_w == "Neumann")
                U[2][icel] = U[2][icel+1*nz];
            else if(SouthWall_w == "Dirichlet")
                U[2][icel] = 2.0*1.0-U[2][icel+1*nz];


            U[2][icel-1*nz] = U[2][icel];
            // =============================== //


            //=====================//
            // North vertical wall //
            //=====================//
            j_index = (ny-3)*nz;
            icel = i*nz*ny + j_index + k;

            // =============================== //
            if(NorthWall_u == "no-slip")
                U[0][icel+1*nz] = 2.0*0.0-U[0][icel];
            else if(NorthWall_u == "Neumann")
                U[0][icel+1*nz] = U[0][icel];
            else if(NorthWall_u == "Dirichlet"){
                U[0][icel+1*nz] = 2.0*1.0-U[0][icel];
            }
            U[0][icel+2*nz] = U[0][icel+1*nz];
            // =============================== //


            // =============================== //
            if(NorthWall_v == "no-slip")
                U[1][icel] = 0.0;
            else if(NorthWall_v == "Neumann")
                U[1][icel] = U[icel-1*nz][1];
            else if(NorthWall_v == "Dirichlet")
                U[1][icel] = 1.0;


            U[1][icel+1*nz] =  U[1][icel];
            U[1][icel+2*nz] = U[1][icel+1*nz];
            // =============================== //

            // =============================== //
            if(NorthWall_w == "no-slip")
                U[2][icel+1*nz] = 2.0*0.0-U[2][icel];
            else if(NorthWall_w == "Neumann")
                U[2][icel+1*nz] = U[2][icel];
            else if(NorthWall_w == "Dirichlet")
                U[2][icel+1*nz] = 2.0*1.0-U[2][icel];


            U[2][icel+2*nz] = U[2][icel+1*nz];
            // =============================== //

        }
    }




    for(size_t i = 0; i < nx; ++i ){
        for(size_t j = 0; j < ny; ++j ){
            

            //====================//
            // Back vertical wall //
            //====================//
            k_index = 1;
            icel = i*nz*ny + j*nz + k_index;

            // =============================== //
            if(BackWall_u == "no-slip")
                U[0][icel] = 2.0*0.0-U[0][icel+1];
            else if(BackWall_u == "Neumann")
                U[0][icel] = U[0][icel+1];
            else if(BackWall_u == "Dirichlet")
                U[0][icel] = 2.0*1.0-U[0][icel+1];


            U[0][icel-1] = U[0][icel];
            // =============================== //


            // =============================== //
            if(BackWall_v == "no-slip")
                U[1][icel] = 2.0*0.0-U[1][icel+1];
            else if(BackWall_v == "Neumann")
                U[1][icel] = U[1][icel+1*nz*ny];
            else if(BackWall_v == "Dirichlet")
                U[1][icel] = 2.0*1.0-U[1][icel+1];


            U[1][icel-1] = U[1][icel];
            // =============================== //


            // =============================== //
            if(BackWall_w == "no-slip")
                U[2][icel] = 0.0;
            else if(BackWall_w == "Neumann")
                U[2][icel] = U[2][icel+1];
            else if(BackWall_w == "Dirichlet")
                U[2][icel] = 1.0;


            U[2][icel-1] = U[2][icel];
            // =============================== //


            //=====================//
            // Front vertical wall //
            //=====================//
            k_index = (nz-3);
            icel = i*nz*ny + j*nz + k_index;

            // =============================== //
            if(FrontWall_u == "no-slip")
                U[0][icel+1] = 2.0*0.0-U[0][icel];
            else if(FrontWall_u == "Neumann")
                U[0][icel+1] = U[0][icel];
            else if(FrontWall_u == "Dirichlet")
                U[0][icel+1] = 2.0*1.0-U[0][icel];


            U[0][icel+2] = U[0][icel+1];
            // =============================== //


            // =============================== //
            if(FrontWall_v == "no-slip")
                U[1][icel+1] = 2.0*0.0-U[1][icel];
            else if(FrontWall_v == "Neumann")
                U[1][icel+1] = U[1][icel];
            else if(FrontWall_v == "Dirichlet")
                U[1][icel+1] = 2.0*1.0-U[1][icel];


            U[1][icel+2] = U[1][icel+1];
            // =============================== //


            // =============================== //
            if(FrontWall_w == "no-slip")
                U[2][icel] = 0.0;
            else if(FrontWall_w == "Neumann")
                U[2][icel] = U[2][icel-1];
            else if(FrontWall_w == "Dirichlet")
                U[2][icel] = 1.0;


            U[2][icel+1] =  U[2][icel];
            U[2][icel+2] = U[2][icel+1];
            // =============================== //

        }
    }





    // ======================================================== //
    //                                                          //
    //        Pressure Boundary conditions calculation          //
    //                                                          //
    // ======================================================== //


    for(size_t j = 0; j < ny; ++j )
    {
        for(size_t k = 0; k < nz; ++k )
        {

            //====================//
            // West vertical wall //
            //====================//
            i_index = 1*nz*ny;
            icel = i_index + j*nz + k;

            // =============================== //
            P[icel] = P[icel+1*nz*ny];
            P[icel-1*nz*ny] = P[icel];
            // =============================== //

            //====================//
            // East vertical wall //
            //====================//
            i_index = (nx-3)*nz*ny;
            icel = i_index + j*nz + k;

            // =============================== //
            P[icel+1*nz*ny] = P[icel];
            P[icel+2*nz*ny] = P[icel+1*nz*ny];
            // =============================== //

        }
    }


    for(size_t i = 0; i < nx; ++i )
    {
        for(size_t k = 0; k < nz; ++k )
        {

            //=====================//
            // South vertical wall //
            //=====================//
            j_index = 1*nz;
            icel = i*nz*ny + j_index + k;
            
            // =============================== //
            P[icel] = P[icel+1*nz];
            P[icel-1*nz] = P[icel];
            // =============================== //


            //=====================//
            // North vertical wall //
            //=====================//
            j_index = (ny-3)*nz;
            icel = i*nz*ny + j_index + k;

            // =============================== //
            P[icel+1*nz] = P[icel];
            P[icel+2*nz] = P[icel+1*nz];
            // =============================== //
            
        }
    }

    for(size_t i = 0; i < nx; ++i )
    {
        for(size_t j = 0; j < ny; ++j )
        {

            //====================//
            // Back vertical wall //
            //====================//
            k_index = 1;
            icel = i*nz*ny + j*nz + k_index;

            // =============================== //
            P[icel] = P[icel+1];
            P[icel-1] = P[icel];
            // =============================== //
            

            //=====================//
            // Front vertical wall //
            //=====================//
            k_index = (nz-3);
            icel = i*nz*ny + j*nz + k_index;

            // =============================== //
            P[icel+1] = P[icel];
            P[icel+2] = P[icel+1];
            // =============================== //

            
        }
    }



    
}