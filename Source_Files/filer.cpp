#include <omp.h>
#include <iostream>
#include <fstream>
#include <vector>

#include "Resolution.h"

using std::vector;
using std::fstream;
using std::ofstream;
using std::string;

void Output_Q_Cpp_PLOT3D
    (
    // ======================================================== //
    int istep,

	vector<double> &P,
	vector<vector<double> > &U2,
	vector<double> &ETA
    
    // ======================================================== //
    )
{

    // ======================================================== //
	vector<vector<vector<double> > > uc (nx-gCells*2,vector<vector<double>>(ny-gCells*2,vector<double> (nz-gCells*2)) );
    vector<vector<vector<double> > > vc (nx-gCells*2,vector<vector<double>>(ny-gCells*2,vector<double> (nz-gCells*2)) );
    vector<vector<vector<double> > > wc (nx-gCells*2,vector<vector<double>>(ny-gCells*2,vector<double> (nz-gCells*2)) );


	int icel;

    int Nblock = 1;
	int tempNX = nx-gCells*2;
	int tempNY = ny-gCells*2;
	int tempNZ = nz-gCells*2;
	int N_total = tempNX*tempNY*tempNZ;

    double temp = 1.0;    // mach, alpha, reyn, time //

    vector<vector<vector<float> > > QUout (nx-gCells*2,vector<vector<float>>(ny-gCells*2,vector<float> (nz-gCells*2)) );



    // ======================================================== //



    for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				icel = (i)*nz*ny + (j+gCells)*nz + (k+gCells);
				uc[i][j][k] = 0.5 * (U2[0][icel+(1*ny*nz)] + U2[0][icel+(gCells*ny*nz)]);

				icel = (i+gCells)*nz*ny + (j)*nz + (k+gCells);
				vc[i][j][k] = 0.5 * (U2[1][icel+(1*nz)] + U2[1][icel+(gCells*nz)]);

				icel = (i+gCells)*nz*ny + (j+gCells)*nz + (k);
				wc[i][j][k] = 0.5 * (U2[2][icel+1] + U2[2][icel+gCells]);

            }
        }
    }

	

	
	static int io;
	ofstream file;
    string filename = "Output/P3D";
	filename += std::to_string(io);
	filename += ".q";
    file.open(filename,ofstream::binary);
	io ++;

	file.write((char *)(&Nblock), sizeof(int));  
	file.write((char *)(&tempNX), sizeof(int));
	file.write((char *)(&tempNY), sizeof(int));
	file.write((char *)(&tempNZ), sizeof(int));

	file.write((char *)(&temp), sizeof(float));
	file.write((char *)(&temp), sizeof(float));
	file.write((char *)(&temp), sizeof(float));
	file.write((char *)(&temp), sizeof(float));





	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{
				icel = (i+gCells)*nz*ny + (j+gCells)*nz + (k+gCells);
				QUout[i][j][k] = P[icel];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				file.write((char *)(&QUout[i][j][k]), sizeof(float));


			}
		}
	}


	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				QUout[i][j][k] = uc[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				file.write((char *)(&QUout[i][j][k]), sizeof(float));

			}
		}
	}



	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				QUout[i][j][k] = vc[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				file.write((char *)(&QUout[i][j][k]), sizeof(float));

			}
		}
	}



	for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{

				QUout[i][j][k] = wc[i][j][k];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				file.write((char *)(&QUout[i][j][k]), sizeof(float));

			}
		}
	}



    for(size_t i = 0; i < nx-gCells*2; ++i )
	{
        for(size_t j = 0; j < ny-gCells*2; ++j )
		{
            for(size_t k = 0; k < nz-gCells*2; ++k)
			{
				icel = (i+gCells)*nz*ny + (j+gCells)*nz + (k+gCells);
				QUout[i][j][k] = ETA[icel];

			}
		}
	}

	for (size_t k = 0; k < nz-gCells*2; ++k) 
	{
		for (size_t j = 0; j < ny-gCells*2; ++j) 
		{ 
			for (size_t i = 0; i < nx-gCells*2; ++i) 
			{

				file.write((char *)(&QUout[i][j][k]), sizeof(float));

			}
		}
	}


    vector<vector<vector<double> > >().swap(uc);
    vector<vector<vector<double> > >().swap(vc);
    vector<vector<vector<double> > >().swap(wc);
    vector<vector<vector<float > > >().swap(QUout);



}