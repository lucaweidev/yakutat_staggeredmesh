#ifndef _PRESSURESOLVERS_INCLUDE_
#define _PRESSURESOLVERS_INCLUDE_

#include <vector>
#include "hyb2_matrix.hpp"



using std::vector;


void SuccessiveOverRelaxation
    (
        double zeta, int itmax, double dt, double omega,

        vector<double> &P,
        vector<vector<double> > &U_star,
        vector<vector<double> > &NEIBcell,

        vector<double> &iDx,
        vector<double> &Dxs,
        vector<double> &iDy,
        vector<double> &Dys,
        vector<double> &iDz,
        vector<double> &Dzs
    
    );


void createPressureMatrix
    (   
      
        MatOps::SparseMatrixHYB<double> &matA,

        vector<double> &iDx,
        vector<double> &Dxs,
        vector<double> &iDy,
        vector<double> &Dys,
        vector<double> &iDz,
        vector<double> &Dzs,
        MPI_tools P_MPI
        
    );

void createBMatrix
   (
    // MPI_tools &U_MPI, const int master, int myid, int nproc,
    double dt,  MPI_tools U_MPI, int myid, int nproc,
    vector<vector<double> > &NEIBcell,
    vector<vector<double> > &U_star,

  
    vector<double> &B_coo_val, 


    vector<double> &iDx,
    vector<double> &Dxs,
    vector<double> &iDy,
    vector<double> &Dys,
    vector<double> &iDz,
    vector<double> &Dzs

    
    );



double product
    (
        vector<double> &a,
        vector<double> &b,
        MPI_tools P_MPI
    );

vector<double> BiCGSTAB
    (
        MPI_tools P_MPI, int myid, int nproc, const int master, size_t num_threads,
        double zeta, int itmax,

        MatOps::SparseMatrixHYB<double> &matA,
    
        vector<double> &B_coo_val
    
    );

#endif