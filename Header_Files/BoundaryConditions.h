#ifndef _BOUNDARYCONDITIONS_
#define _BOUNDARYCONDITIONS_

#include <vector>
using std::vector;


void BoundaryConditions(
    
    vector<vector<double> > &U,
    vector<double> &P

);

    

#endif