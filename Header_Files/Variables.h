#ifndef _VARIABLES_
#define _VARIABLES_

    //Unequal grid
    // ======================================================== //

    const double GridderXc = 5;

    const double GridderYc = 5;

    const double GridderZc = 5;

    const int nxSml = 50;

    const int nySml = 50;

    const int nzSml = 50;

    const double lxSml = 1;

    const double lySml = 1;

    const double lzSml = 1;

    double dySml, dy;

    double dxSml, dx;
    
    double dzSml, dz;

    // ======================================================== //


    //OPENMP
    // ======================================================== //

    int nthreads;

    // ======================================================== //


    //Physical variable
    // ======================================================== //

    double nu;

    double Re;

    double dt;

    // ======================================================== //



    //other
    // ======================================================== //

    double zeta_vel;

    double zeta, time, itmax;

    int isto, istep, nstep;

    double omega;
    
    // ======================================================== //

    
    //MPI variables
    // ======================================================== //
    int myid, itag, istart, iend, icount, nproc, lnbr, rnbr, islice;
    const int master = 0;
    double clock, clock1, clock2;
    // ======================================================== //

    //OMP variables
    // ======================================================== //
    size_t num_threads;
    // ======================================================== //

#endif