# Makefile 

# Nom du compilateur
CC = mpiicpc

# Options de compilation: optimisation, debug etc...
OPT = -O2 -fopenmp -std=c++11 -march=native -mcmodel=medium 
Linking = 
INC = -I Header_Files/
# -ipo -xCORE-AVX2 -align array32byte 
# -mcmodel=large
# -heap-arrays 64 
# --coverage

EXE = yakutat

DIR_OBJ = ./obj
DIR_BIN = ./bin

# Defining the objects (OBJS) variables  P.S. If u want to add cpp file, u can modify here.
OBJS =  \
    main.o \
	gridder.o \
	readingData.o \
	BoundaryConditions.o \
	filer.o \
	ConvectionScheme.o \
	PressureSolvers.o \
	calNewVelocity.o \
	MPI_tools.o \

# Linking object files
exe :  $(OBJS) 
	$(CC) -o $(EXE) \
    $(OBJS) \
    $(OPT) $(Linking)


# echo something
	@echo "   ***************** successful *****************   "                                                                                      
	@echo "    |  Author:  Zi-Hsuan Wei                        "                       
	@echo "    |  Version: 1.0                                 "                   
	@echo "    |  Web:     http://smetana.me.ntust.edu.tw/     "
	@echo "    |  Editor : Hsuan                               "  

# Defining the flags of objects
%.o: Source_Files/%.cpp
	@$(CC) $< $(OPT) $(INC) -c 


# Removing object files
clean :
	@/bin/rm -f Output/*.dat
	@/bin/rm -f Output/*.x
	@/bin/rm -f Output/*.q

cleanall : 
	@/bin/rm -f $(OBJS) $(EXE)  *.mod
	@/bin/rm -f Output/*.dat
	@/bin/rm -f Output/*.x
	@/bin/rm -f Output/*.q
    
config :
	if [ ! -d obj ] ; then mkdir Output ; fi


